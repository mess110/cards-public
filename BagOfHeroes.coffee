BagOfHeroes =
  babaDochia:
    id: "babaDochia"
    displayName: "Baba Dochia"
    cost: 0
    damage: ""
    health: 21
    onPlay: []
    deathRattle: []
    frozen: 0

  ileanaCosanzeana:
    id: "ileanaCosanzeana"
    displayName: "Ileana Cosânzeana"
    cost: 0
    damage: ""
    health: 21
    onPlay: []
    deathRattle: []
    frozen: 0

  zalmoxis:
    id: "zalmoxis"
    displayName: "Zalmoxis"
    cost: 0
    damage: ""
    health: 21
    onPlay: []
    deathRattle: []
    frozen: 0

  gerila:
    id: "gerila"
    displayName: "Gerilă"
    cost: 0
    damage: ""
    health: 21
    onPlay: []
    deathRattle: []
    frozen: 0

  find: (json) ->
    this[json['id']]

exports.BagOfHeroes = BagOfHeroes
