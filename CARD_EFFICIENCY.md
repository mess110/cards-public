Assumptions
===========

from https://www.elie.net/blog/hearthstone/how-to-appraise-hearthstone-card-values

1. The mana cost of a card is proportional to the card power: This assumption
models the fact that a two-mana card is more powerful than a one-mana card. If
this were not true, then there would be a one-mana card more powerful than a
two-mana card. (This would lead a player to only play those cheaper one-mana
cards.)

2. The power of cards increases roughly linearly: This is probably the
trickiest assumption of the model. It implies that a two-mana card is roughly
twice as powerful as a one-mana card, and a three-mana card is three times more
powerful than a one-mana card. The reasoning behind this assumption is the
following: if Hearthstone cards’ power were exponential then any late game card
(e.g. 7 mana) would outpower a board of early game cards (e.g. 1 and 2 mana).
Given the success of decks such as the Zoo deck, this is clearly not the case.
Similarly, if the power of cards increases sublinearly then any late game
(control deck) would have no chance of winning. This is clearly not the case
either because control decks also can be used to win (e.g. control warrior). We
will see in a later post that game metrics behave in a way which is consistent
with this hypothesis.

3. Card effects have a constant price: This implies that an effect such as
charge has the same cost no matter if it’s a one-mana card, a two-mana card, or
a ten-mana card. Similarly, a +3 spell power has the same cost regardless of
the cost of the card. However, following assumption #2, a +2 spell power costs
twice as much as a a +1 spell power. This applies also to minion health,
attack, and so forth.

4. A card has an intrinsic value: Given than the number of cards drawn is
constrained by the game, the simple fact of holding a card gives a player an
advantage. This advantage is captured by adding an intrinsic (constant) value
to cards.

5. The cost of a card is the sum of its attributes: This assumption implies
that the mana cost of a card is exclusively based on what the card attributes
are. Given that Hearthstone is a closed system (no external force) that
apparently values rationality, there’s no reason to account for unknown or
hidden variables that would affect the game.

c - the cost of the card
d - damage of the card
h - health of the card
i - imaginary value of the card (ie: for holding it)

4 = 4d + 7h + i
