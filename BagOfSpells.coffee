BagOfSpells =
  meditate:
    ownMaxMana: 1
  evilEye:
    enemyMaxMana: -2
  focus:
    ownCurrentMana: 1
  lucky:
    ownDrawCards: 1
  unlucky:
    enemyDrawCards: 2
  breathOfFreshAir:
    ownHealth: 2
    enemyHealth: -5
  holyNova:
    ownHealth: 1
    enemyHealth: -1
    ownPlayedCardsHealth: 1
    enemyPlayedCardsHealth: -1
  heal:
    targetHealth: 4
  rage:
    targetHealth: -1
    targetDamage: 2
  moonfire:
    targetHealth: -1
  fireball:
    targetHealth: -6
  pyroblast:
    targetHealth: -10
  freeze:
    targetFreeze: 1
  poison:
    enemyHealth: -2
    ownHealth: 2
  sting:
    targetHealth: -1
  crowDeath:
    enemyHealth: -2

  passiveFireball:
    targetHealth: -1
  passiveHeal:
    targetHealth: 1

exports.BagOfSpells = BagOfSpells
