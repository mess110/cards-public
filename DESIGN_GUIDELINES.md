Goals
-----

* fantasy
* challenge
* fellowship
* discovery
* submission (grind/repetition)
* expression
* narrative
* sensory pleasure

Card
----

* needs to have gradient on border so they look ok stacked

Webapp
------

Login screen

* WoW Login Screen like

Register Screen

* Wow Login Screen like

Menu
----

* Magic the gathering like with 2 boxes: single player and multiplayer

Menu > Single player
--------------------

* Hearthone like hero select
* Start button

Menu > Multiplayer
------------------

* Hearthone like hero select
* number of players in queue
* searching for player Hearthstone like

Game
----

Assets
------

* logo
* card back
* card front
* font (light color with dark border)
* mouse cursor
* mouse cursor spell
* 4 hero cards tilted
* n cards not tilted
* end turn button
* end turn button back
* board
* border (used as "player table")
* damage sign
* attack sign
* background
* login screen background image
