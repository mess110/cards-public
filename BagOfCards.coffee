# TODO think of better name
# http://ro.wikipedia.org/wiki/Mitologie_rom%C3%A2neasc%C4%83
BagOfCards =
  solomonar:
    id: "solomonar"
    displayName: "Solomonar"
    cost: 4
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  vidra:
    id: "vidra"
    displayName: "Vidra"
    cost: 6
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  urs:
    id: "urs"
    displayName: "Urs"
    cost: 3
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  strigoi:
    id: "strigoi"
    displayName: "Strigoi"
    cost: 3
    damage: 3
    health: 4
    onPlay: []
    deathRattle: []
    frozen: 0

  vasilisc:
    id: "vasilisc"
    displayName: "Vasilisc"
    cost: 1
    damage: 1
    health: 1
    onPlay: []
    deathRattle: []
    frozen: 0

  # heal
  luceafar:
    id: "luceafar"
    displayName: "Luceafăr"
    cost: 8
    damage: 8
    health: 9
    onPlay: []
    deathRattle: []
    frozen: 0

  calulNazdravan:
    id: "calulNazdravan"
    displayName: "Calul Năzdrăvan"
    cost: 4
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  pasareaMaiastra:
    id: "pasareaMaiastra"
    displayName: "Pasărea Măiastră"
    cost: 5
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  pricolici:
    id: "pricolici"
    displayName: "Pricolici"
    cost: 2
    damage: 3
    health: 2
    onPlay: ["passiveHeal"]
    deathRattle: []
    frozen: 0

  albina:
    id: "albina"
    displayName: "Albină"
    cost: 1
    damage: 1
    health: 1
    onPlay: []
    deathRattle: ["lucky"]
    frozen: 0

  viespe:
    id: "viespe"
    displayName: "Viespe"
    cost: 2
    damage: 3
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  reginaAlbinelor:
    id: "reginaAlbinelor"
    displayName: "Regina Albinelor"
    cost: 5
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  sancioara:
    id: "sancioara"
    displayName: "Sâncioară"
    cost: 4
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  samca:
    id: "samca"
    displayName: "Samca"
    cost: 4
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  furnica:
    id: "furnica"
    displayName: "Furnică"
    cost: 1
    damage: 1
    health: 1
    onPlay: []
    deathRattle: []
    frozen: 0

  reginaFurnicilor:
    id: "reginaFurnicilor"
    displayName: "Regina Furnicilor"
    cost: 7
    damage: 6
    health: 4
    onPlay: []
    deathRattle: []
    frozen: 0

  zgriptor:
    id: "zgriptor"
    displayName: "Zgripțor"
    cost: 9
    damage: 6
    health: 12
    onPlay: []
    deathRattle: []
    frozen: 0

  zorila:
    id: "zorila"
    displayName: "Zorilă"
    cost: 8
    damage: 8
    health: 9
    onPlay: []
    deathRattle: []
    frozen: 0

  zmeu:
    id: "zmeu"
    displayName: "Zmeu"
    cost: 9
    damage: 10
    health: 9
    onPlay: []
    deathRattle: []
    frozen: 0

  balaur:
    id: "balaur"
    displayName: "Balaur"
    cost: 6
    damage: 3
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  capcaun:
    id: "capcaun"
    displayName: "Căpcăun"
    cost: 8
    damage: 7
    health: 7
    onPlay: []
    deathRattle: []
    frozen: 0

  ceasRau:
    id: "ceasRau"
    displayName: "Ceas Rău"
    cost: 7
    damage: 5
    health: 4
    onPlay: []
    deathRattle: []
    frozen: 0

  iele:
    id: "iele"
    displayName: "Iele"
    cost: 5
    damage: 3
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  muma:
    id: "muma"
    displayName: "Muma Pădurii"
    cost: 10
    damage: 7
    health: 7
    onPlay: []
    deathRattle: []
    frozen: 0

  curaj:
    id: "curaj"
    displayName: "Curaj"
    cost: 7
    damage: 5
    health: 4
    onPlay: []
    deathRattle: []
    frozen: 0

  sanziene:
    id: "sanziene"
    displayName: "Sânziene"
    cost: 5
    damage: 4
    health: 1
    onPlay: []
    deathRattle: []
    frozen: 0

  spiridus:
    id: "spiridus"
    displayName: "Spiriduș"
    cost: 1
    damage: 1
    health: 1
    onPlay: []
    deathRattle: []
    frozen: 0

  schtimaApelor:
    id: "schtimaApelor"
    displayName: "Știma Apelor"
    cost: 7
    damage: 4
    health: 3
    onPlay: []
    deathRattle: []
    frozen: 0

  # http://ro.wikipedia.org/wiki/Ursitoare

  # hotaraste soarta omului la nastere
  ursitoarea:
    id: "ursitoarea"
    displayName: "Ursitoare"
    cost: 3
    damage: 1
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  # prezice destinul
  soarta:
    id: "soarta"
    displayName: "Soarta"
    cost: 3
    damage: 1
    health: 2
    onPlay: ["meditate"]
    deathRattle: []
    frozen: 0

  # curma viata.. duhh
  moartea:
    id: "moartea"
    displayName: "Moartea"
    cost: 3
    damage: 1
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  stafie:
    id: "stafie"
    displayName: "Stafie"
    cost: 2
    damage: 3
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  urias:
    id: "urias"
    displayName: "Uriaș"
    cost: 8
    damage: 3
    health: 6
    onPlay: []
    deathRattle: []
    frozen: 0

  varcolac:
    id: "varcolac"
    displayName: "Vârcolac"
    cost: 2
    damage: 2
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  zana:
    id: "zana"
    displayName: "Zână"
    cost: 2
    damage: 3
    health: 2
    onPlay: []
    deathRattle: []
    frozen: 0

  duh:
    id: "duh"
    displayName: "Duh"
    cost: 1
    damage: 1
    health: 1
    onPlay: ["freeze"]
    deathRattle: []
    frozen: 0

  cioara:
    id: "cioara"
    displayName: "Cioara"
    cost: 1
    damage: 1
    health: 1
    onPlay: []
    deathRattle: []
    frozen: 0

  find: (json) ->
    # throw 'card with id "' + json['id'] + '" not found' if this.keys().indexOf(json['id']) == -1
    this[json['id']]

  writeDescription: (h) ->
    h.description = ""
    for s in h.onPlay
      h.description += "#{s}"
    for s in h.deathRattle
      h.description += "#{s}"
    h

  toArray: (keys) ->
    a = []
    for key in keys
      a.push @writeDescription(BagOfCards[key])
    a

exports.BagOfCards = BagOfCards
